require "log"
require "crinja"

require "./app/db"
require "./app/minecraft"
require "./app/hooks"
require "./app/raven"
require "./models/*"

class Time::Location
  def self.local
    Location::UTC
  end
end

module Holyblocks
  def self.current_user
    "tester"
  end

  def self.authenticate_user(ctx)
    ENV["KEMAL_ENV"]? == "development"
  end

  class_getter base_url = URI.parse("https://admin.holyblocks.de/")

  class_getter credentials = {
    "minecraft" => {
      "host" => "mc.holyblocks.de",
      "port" => "10530",
    },
    "teamspeak" => {
      "host" => "ts.holyblocks.de",
      "port" => "12550",
      "server_password" => "1PetrKzidlS2dvdMv4avGaugwi",
    }
  }
end

module Pwgen
  VERSION = "0.1.0"

  def self.generate(*, size = 16, charset = "234679ACDEFGHJKMNPQRTWXYZabcdefghjkmnopqrstwxy", random = Random::Secure)
    charset = charset.chars

    String.build(size) do |io|
      size.times do
        io << charset.sample(random)
      end
    end
  end
end
