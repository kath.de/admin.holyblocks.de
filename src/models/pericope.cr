@[Crinja::Attributes]
class Pericope
  include Crinja::Object::Auto
  include Clear::Model

  record Verse, chapter : Int32, verse : Int32 do
    include Crinja::Object::Auto
    def self.parse(string : String)
      chapter, _, verse = string.partition(',')
      new chapter.to_i, verse.to_i
    end

    def to_s(io : IO)
      io << chapter << ',' << verse
    end

    def to_json(builder)
      builder.string to_s
    end
  end

  class VerseConverter
    def self.to_column(x) : Verse?
      case x
      when Nil
        nil
      when Slice(UInt8)
        Verse.parse(String.new(x))
      when String
        Verse.parse(x)
      else
        raise "Cannot convert from #{x.class} to Verse"
      end
    end

    def self.to_db(v : Verse?)
      v.try(&.to_s)
    end
  end

  Clear::Model::Converter.add_converter("verse_converter", VerseConverter)

  primary_key
  timestamps

  belongs_to book : Book
  column book_serial : Int32, presence: false
  column world_handle : String, presence: false

  column title : String
  column labels : Array(String)
  column verse_start : Verse, converter: "verse_converter"
  column verse_end : Verse, converter: "verse_converter"
  column level : Int32
  column author : String

  column library_location : String?

  belongs_to group : Group?
  column assigned_at : Time?
  column finished_at : Time?

  scope(:assignable) do
    where("group_id IS NULL").order_by("created_at")
    # TODO: .where("library_location IS NOT NULL")
  end

  scope(:with_level) do |level|
    where("level = ?", [level])
  end

  before :create, assign_serial

  def assign_serial
    self.book_serial = book.max_serial + 1
    self.world_handle = "#{book.abbr}_p#{book_serial}"
  end

  def pericope_id
    world_handle
  end

  def access_portal_handle
    "#{world_handle}_hin"
  end

  def bibleserver_url
    "https://bibleserver.com/EU/#{ book.bibleserver_name }#{ URI.encode_www_form(verses(only_one_chapter: true)) }"
  end

  def verses(only_one_chapter = false) : String
    if verse_start.chapter == verse_end.chapter
      "#{verse_start}-#{verse_end.verse}"
    elsif only_one_chapter
      verse_start.to_s
    else
      "#{verse_start}-#{verse_end}"
    end
  end

  def to_s(io : IO)
    io << book.abbr << " " << verses
  end

  def self.all_labels
    Clear::SQL.select("unnest(labels) AS label").distinct.from("pericopes").to_a
  end

  def assign_to(group : Group)
    self.group = group
    self.assigned_at = Time.utc
    self.save!
  end

  def unassign
    self.group = nil
    self.assigned_at = nil
    self.save!
  end
end
