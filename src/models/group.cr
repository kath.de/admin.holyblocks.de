class Group
  include Clear::Model
  include Crinja::Object::Auto

  primary_key
  timestamps

  column name : String
  column mail : String
  column manage_token : String
  column credentials : JSON::Any
  column minecraft_handle : String, presence: false

  has_many pericopes : Pericope
  has_many accounts : Account

  before :validate, assign_token
  before :validate, assign_credentials

  before :create, assign_minecraft_handle

  def assign_token
    unless manage_token_column.defined?
      self.manage_token = Random::Secure.urlsafe_base64(16)
    end
  end

  def assign_credentials
    unless credentials_column.defined?
      self.credentials = JSON::Any.new({
        "minecraft" => JSON::Any.new({
          "group" => JSON::Any.new(name),
        }),
        "teamspeak" => JSON::Any.new({
          "channel" => JSON::Any.new(name),
          "password" => JSON::Any.new(Pwgen.generate),
        })
      })
    end
  end

  def assign_minecraft_handle
    self.minecraft_handle = name.underscore.gsub(' ', '_').gsub('.', '_').gsub(/__+/, '_').gsub("ß", "ss").tr("äöü", "aou")
  end

  @[Crinja::Attribute]
  def manage_path
    "/groups/manage/#{manage_token}"
  end

  @[Crinja::Attribute]
  def manage_url
    Holyblocks.base_url.resolve(manage_path).to_s
  end

  def crinja_attribute(attr : ::Crinja::Value) : ::Crinja::Value
    {% begin %}
      {% exposed = [] of _ %}
      value = case attr.to_string
      {% for db_column_name, settings in COLUMNS %}
        {% var_name = settings[:crystal_variable_name] %}
        when {{ var_name.stringify }}
          self.{{ var_name }}
      {% end %}
      else
        super
      end

      ::Crinja::Value.new(value)
    {% end %}
  end
end
