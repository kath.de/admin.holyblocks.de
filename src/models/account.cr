@[Crinja::Attributes]
class Account
  include Clear::Model
  include Crinja::Object::Auto

  primary_key
  timestamps

  column uuid : UUID
  column user_handle : String
  column real_name : String?

  belongs_to group : Group
end
