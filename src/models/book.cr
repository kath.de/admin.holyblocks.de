@[Crinja::Attributes]
class Book
  include ::Crinja::Object::Auto
  include Clear::Model

  primary_key
  timestamps

  column name : String
  column abbr : String
  column long_name : String
  column bibleserver_name : String
  reserved_for : String?
  column created_at : Time, presence: false

  has_many pericopes : Pericope

  @pericopes_count : Int64?
  def pericopes_count
    @pericopes_count ||= Clear::SQL.select("COUNT(*)").from("pericopes").where("book_id = ?", [id]).scalar(Int64)
  end

  def crinja_attribute(attr : ::Crinja::Value) : ::Crinja::Value
    {% begin %}
      {% exposed = [] of _ %}
      value = case attr.to_string
      {% for db_column_name, settings in COLUMNS %}
        {% var_name = settings[:crystal_variable_name] %}
        when {{ var_name.stringify }}
          self.{{ var_name }}
      {% end %}
      else
        #::Crinja::Undefined.new(attr.to_s)
        super
      end

      ::Crinja::Value.new(value)
    {% end %}
  end

  def max_serial
    Clear::SQL.select("MAX(book_serial)").from("pericopes").where("book_id = ?", [id]).scalar(Int32?) || 0
  end
end
