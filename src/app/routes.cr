require "kemal"
require "sass"
require "./mojang"
require "./mail"

macro authenticate_user!(ctx)
  unless Holyblocks.authenticate_user({{ ctx }})
    {{ ctx }}.response.respond_with_status 401
    next
  end
end

get "/books" do |ctx|
  authenticate_user!(ctx)

  page = Page.new
  page.context["books"] = Book.query.order_by("id").to_a
  page.render ctx.response, "books/index.html.j2"
end

get "/books/:id" do |ctx|
  authenticate_user!(ctx)

  page = Page.new
  book = Book.query.find!({id: ctx.params.url["id"].to_i})
  page.context["book"] = book
  pericopes = book.pericopes.order_by("created_at")
  page.context["pericopes"] = pericopes.to_a
  page.context["current_user"] = Holyblocks.current_user
  page.context["all_labels"] = Pericope.all_labels
  page.render ctx.response, "books/show.html.j2"
end

post "/pericopes" do |ctx|
  authenticate_user!(ctx)

  pericope = Pericope.new
  pericope.book = Book.query.find!({id: ctx.params.body["book_id"].to_i})
  pericope.title = ctx.params.body["title"]
  pericope.labels = ctx.params.body["labels"].split(',').map(&.strip)
  pericope.verse_start = Pericope::Verse.parse(ctx.params.body["verse_start"])
  pericope.verse_end = Pericope::Verse.parse(ctx.params.body["verse_end"])
  pericope.level = ctx.params.body["level"].to_i
  pericope.author = Holyblocks.current_user
  pericope.save!

  ctx.redirect "/books/#{pericope.book.id}"
end

get "/groups" do |ctx|
  authenticate_user!(ctx)

  page = Page.new
  page.context["groups"] = Group.query.order_by("created_at").to_a
  page.render ctx.response, "groups/index.html.j2"
end

get "/groups/new" do |ctx|
  authenticate_user!(ctx)

  page = Page.new
  page.render ctx.response, "groups/new.html.j2"
end

post "/groups" do |ctx|
  authenticate_user!(ctx)

  group = Group.new
  group.name = ctx.params.body["name"]
  group.mail = ctx.params.body["mail"]
  group.save!

  Minecraft::Server.connect do |client|
    Holyblocks::Hooks.group_created(client, group)
  end

  Holyblocks::Mail.send_group_created(group)

  ctx.redirect "/groups/#{group.id}"
end

get "/groups/:id" do |ctx|
  authenticate_user!(ctx)

  page = Page.new
  group = Group.query.find!({id: ctx.params.url["id"].to_i})
  page.context["group"] = group
  pericopes = group.pericopes.order_by("assigned_at")
  page.context["pericopes"] = pericopes.to_a

  raw_level = ctx.params.query["level"]? || "any"
  pericope_suggestions = Pericope.assignable
  if raw_level != "any"
    pericope_suggestions = pericope_suggestions.with_level(raw_level.to_i)
  end
  page.context["pericope_suggestions"] = pericope_suggestions.to_a
  page.context["level"] = raw_level

  page.render ctx.response, "groups/show.html.j2"
end

get "/groups/manage/:token" do |ctx|
  page = Page.new
  group = Group.query.find!({manage_token: ctx.params.url["token"]})
  page.context["group"] = group
  pericopes = group.pericopes.order_by("assigned_at")
  page.context["pericopes"] = pericopes.to_a
  page.context["credentials"] = Holyblocks.credentials
  page.context["accounts"] = group.accounts.to_a

  page.render ctx.response, "groups/manage.html.j2"
end

# Assign a pericope to a group
post "/groups/:id/pericopes" do |ctx|
  authenticate_user!(ctx)

  group = Group.query.find!({id: ctx.params.url["id"].to_i})
  pericope = Pericope.query.find!({id: ctx.params.body["pericope_id"].to_i})

  pericope.assign_to group

  Minecraft::Server.connect do |client|
    Holyblocks::Hooks.pericope_assigned client, group, pericope
  end

  Holyblocks::Mail.send_pericope_assigned(group, pericope)

  ctx.redirect "/groups/manage/#{ group.manage_token}"
end

# Add a user to a group
post "/groups/manage/:token/users" do |ctx|
  group = Group.query.find!({manage_token: ctx.params.url["token"]})
  user_handle = ctx.params.body["user_handle"]

  begin
    uuid = UUID.new(user_handle)
    user_handle = nil
  rescue ArgumentError
    uuid = Mojang.get_uuid(user_handle.not_nil!)
  end

  account = Account.new
  account.uuid = uuid
  account.user_handle = user_handle || uuid.to_s
  account.real_name = ctx.params.body["real_name"]?

  group.accounts << account

  Minecraft::Server.connect do |client|
    Holyblocks::Hooks.add_account client, group, account
  end

  ctx.redirect "/groups/manage/#{ group.manage_token}"
end

# Remove a user from a group
delete "/groups/manage/:token/users/:user_handle" do |ctx|
  group = Group.query.find!({manage_token: ctx.params.url["token"]})

  account = Account.query.find!({uuid: ctx.params.url["uuid"]})

  group.accounts.unlink account

  Minecraft::Server.connect do |client|
    Holyblocks::Hooks.remove_account client, group, account
  end

  ctx.redirect "/groups/manage/#{ group.manage_token}"
end

css_cache = nil

get "/style.css" do |context|
  context.response.headers["Content-Type"] = "text/css"

  #css_cache ||= begin
    Sass.compile_file("app/sass/main.sass", is_indented_syntax_src: true, include_path: "app/sass/")
  #end
end
