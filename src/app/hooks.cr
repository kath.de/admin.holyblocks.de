module Holyblocks::Hooks
  def self.group_created(client, group : Group)
    client.command "lp creategroup #{group.minecraft_handle}"
    client.command "lp group #{group.minecraft_handle} parent add bibelbauer"

    client.command "lp group #{group.minecraft_handle} permission set prefix.2.&b[#{group.name.gsub(']', "")}]"
    client.command "lp group #{group.minecraft_handle} setdisplayname #{group.name}"
  end

  def self.add_account(client, group, account)
    client.command "lp user #{account.user_handle} parent add #{group.minecraft_handle}"
    # if group_admin
    #   client.command "lp user #{account.user_handle} parent add katecheten"
    # end
  end

  def self.remove_account(client, group, account)
    client.command "lp user #{account.user_handle} parent remove #{group.minecraft_handle}"
  end

  def self.pericope_assigned(client, group, pericope)
    client.command "lp group alle permission set multiverse.portal.access.#{ pericope.access_portal_handle } value=false"
    client.command "lp group #{group.minecraft_handle} permission set multiverse.portal.access.#{ pericope.access_portal_handle }"
  end

  def self.pericope_unassigned(client, group, pericope)
    client.command "lp group alle permission unset multiverse.portal.access.#{ pericope.access_portal_handle }"
    client.command "lp group #{group.minecraft_handle} permission unset multiverse.portal.access.#{ pericope.access_portal_handle }"
  end
end
