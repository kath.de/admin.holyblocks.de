require "rconcr"
require "uri"

struct Minecraft::Server
  Log = ::Log.for("minecraft.rcon")

  class_property uri : URI { URI.parse(ENV["MINECRAFT_RCON_URL"]) }

  def self.connect
    RCON::Client.open(uri) do |client|
      yield new(client)
    end
  end

  getter client

  def initialize(@client : RCON::Client)
  end

  def command(command)
    Log.info { command }
    response = client.command command
    if response != ""
      raise "Unexpected RCON response: #{response.inspect} for #{command.inspect}"
    end
  end
end

struct Minecraft::Permissions
  class_property uri : URI { URI.parse(ENV["MINECRAFT_LUCKPERMS_URL"]) }

  class_getter db : DB { DB.open(uri) }

  def self.connect
    db.using_connection do |connection|
      yield new(connection)
    end
  end

  def self.transaction
    db.transaction do |transaction|
      yield new(transaction.connection), transaction
    end
  end

  def initialize(@connection : DB::Connection)
  end

  record Account, username : String

  def accounts(group : Group)
  end
end
