require "http/client"
require "json"
require "uuid"

module Mojang
  def self.get_uuid(username : String) : UUID
    response = HTTP::Client.get("https://api.mojang.com/users/profiles/minecraft/#{username}")
    if response.status.success?
      json = JSON.parse(response.body)
      UUID.new(json["id"].as_s)
    else
      raise "Invalid API response: #{response}"
    end
  end
end
