# append to your require list on top:
require "clear"

# initialize a pool of database connection:
db_url = if ENV["KEMAL_ENV"]? == "test"
           ENV["TEST_DATABASE_URL"]
else
    ENV["DATABASE_URL"]
end

Clear::SQL.init(db_url, connection_pool_size: 5)

module Holyblocks
end
