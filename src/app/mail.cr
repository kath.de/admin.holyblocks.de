require "email"

module Holyblocks::Mail
  class_getter crinja : Crinja = initialize_crinja

  class_getter config : EMail::Client::Config do
    config = EMail::Client::Config.new("mail.kath.de", 587)
    config.use_tls(:starttls)
    config.use_auth("helferlein@holyblocks.de", ENV["MAIL_PASSWORD"])
    config
  end

  def self.initialize_crinja
    crinja = Crinja.new
    crinja.loader = Crinja::Loader::FileSystemLoader.new("app/mails/")
    crinja
  end

  def self.send_group_created(group)
    email = EMail::Message.new
    email.from    "helferlein@holyblocks.de"
    email.to      group.mail
    rendered = crinja.get_template("group-created-manage.md.j2").render({
      "group" => group
    })
    subject, _, body = rendered.partition("\n\n")
    email.subject subject
    email.message body

    client = EMail::Client.new(config)
    client.start do
      client.send(email)
    end
  end

  def self.send_pericope_assigned(group, pericope)
    email = EMail::Message.new
    email.from    "helferlein@holyblocks.de"
    email.to      group.mail
    rendered = crinja.get_template("pericope-assigned.md.j2").render({
      "group" => group,
      "pericope" => pericope,
      "credentials" => Holyblocks.credentials
    })
    subject, _, body = rendered.partition("\n\n")

    email.subject subject
    email.message body

    client = EMail::Client.new(config)
    client.start do
      client.send(email)
    end
  end
end
