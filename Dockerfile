FROM crystallang/crystal:0.35.1 AS builder

RUN apt-get update && apt-get install \
# bash needed for dokku enter
      bash \
      wget \
      libsass-dev

RUN wget -qO /usr/local/bin/dbmate https://github.com/amacneil/dbmate/releases/download/v1.7.0/dbmate-linux-musl-amd64 \
  && chmod +x /usr/local/bin/dbmate

WORKDIR /src
ADD shard.yml shard.lock ./
RUN shards install --production

ADD . ./

RUN make build
