# admin.holyblocks.de

TODO: Write a description here

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     admin.holyblocks.de:
       github: your-github-user/admin.holyblocks.de
   ```

2. Run `shards install`

## Usage

```crystal
require "admin.holyblocks.de"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://github.com/your-github-user/admin.holyblocks.de/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Johannes Müller](https://github.com/your-github-user) - creator and maintainer
