require "crinja"
require "../src/app/db"
require "../src/models/*"

books = [
  Book.new({name: "Genesis", id: 1, abbr: "Gen", long_name: "Das Buch Genesis"}),
  Book.new({name: "Exodus", id: 2, abbr: "Ex", long_name: "Das Buch Exodus"}),
  Book.new({name: "Levitikus", id: 3, abbr: "Lev", long_name: "Das Buch Levitikus"}),
  Book.new({name: "Numeri ", id: 4, abbr: "Num", long_name: "Das Buch Numeri"}),
  Book.new({name: "Deuteronomium", id: 5, abbr: "Dtn", long_name: "Das Buch Deuteronomium"}),
  Book.new({name: "Josua", id: 6, abbr: "Jos", long_name: "Das Buch Josua"}),
  Book.new({name: "Richter ", id: 7, abbr: "Ri", long_name: "Das Buch Richter"}),
  Book.new({name: "Rut", id: 8, abbr: "Rut", long_name: "Das Buch Rut"}),
  Book.new({name: "Das erste Buch Samuel", id: 9, abbr: "1 Sam", long_name: "Das erste Buch Samuel"}),
  Book.new({name: "Das zweite Buch Samuel", id: 10, abbr: "2 Sam", long_name: "Das zweite Buch Samuel"}),
  Book.new({name: "Das erste Buch der Könige", id: 11, abbr: "1 Kön", long_name: "Das erste Buch der Könige"}),
  Book.new({name: "Das zweite Buch der Könige", id: 12, abbr: "2 Kön", long_name: "Das zweite Buch der Könige"}),
  Book.new({name: "Das erste Buch der Chronik", id: 13, abbr: "1 Chr", long_name: "Das erste Buch der Chronik"}),
  Book.new({name: "Das zweite Buch der Chronik", id: 14, abbr: "2 Chr", long_name: "Das zweite Buch der Chronik"}),
  Book.new({name: "Esra", id: 15, abbr: "Esra", long_name: "Das Buch Esra"}),
  Book.new({name: "Nehemia", id: 16, abbr: "Neh", long_name: "Das Buch Nehemia"}),
  Book.new({name: "Tobit", id: 17, abbr: "Tob", long_name: "Das Buch Tobit"}),
  Book.new({name: "Judit", id: 18, abbr: "Jdt", long_name: "Das Buch Judit"}),
  Book.new({name: "Ester", id: 19, abbr: "Est", long_name: "Das Buch Ester"}),
  Book.new({name: "Das erste Buch der Makkabäer", id: 20, abbr: "1 Makk", long_name: "Das erste Buch der Makkabäer"}),
  Book.new({name: "Das zweite Buch der Makkabäer", id: 21, abbr: "2 Makk", long_name: "Das zweite Buch der Makkabäer"}),
  Book.new({name: "Ijob", id: 22, abbr: "Ijob", long_name: "Das Buch Ijob"}),
  Book.new({name: "Psalmen", id: 23, abbr: "Ps", long_name: "Das Buch der Psalmen"}),
  Book.new({name: "Sprichwörter", id: 24, abbr: "Spr", long_name: "Das Buch der Sprichwörter"}),
  Book.new({name: "Kohelet (Ekklesiastes)", id: 25, abbr: "Koh", long_name: "Das Buch Kohelet (Ekklesiastes)"}),
  Book.new({name: "Das Hohelied", id: 26, abbr: "Hld", long_name: "Das Hohelied"}),
  Book.new({name: "Das Buch Weisheit", id: 27, abbr: "Weish", long_name: "Das Buch Weisheit"}),
  Book.new({name: "Jesus Sirach", id: 28, abbr: "Sir", long_name: "Das Buch Jesus Sirach"}),
  Book.new({name: "Jesaja", id: 29, abbr: "Jes", long_name: "Das Buch Jesaja"}),
  Book.new({name: "Jeremia", id: 30, abbr: "Jer", long_name: "Das Buch Jeremia"}),
  Book.new({name: "Das Buch der Klagelieder", id: 31, abbr: "Klgl", long_name: "Das Buch der Klagelieder"}),
  Book.new({name: "Baruch", id: 32, abbr: "Bar", long_name: "Das Buch Baruch"}),
  Book.new({name: "Ezechiel", id: 33, abbr: "Ez", long_name: "Das Buch Ezechiel"}),
  Book.new({name: "Daniel", id: 34, abbr: "Dan", long_name: "Das Buch Daniel"}),
  Book.new({name: "Hosea", id: 35, abbr: "Hos", long_name: "Das Buch Hosea"}),
  Book.new({name: "Joel", id: 36, abbr: "Joel", long_name: "Das Buch Joel"}),
  Book.new({name: "Amos", id: 37, abbr: "Am", long_name: "Das Buch Amos"}),
  Book.new({name: "Obadja", id: 38, abbr: "Obd", long_name: "Das Buch Obadja"}),
  Book.new({name: "Jona", id: 39, abbr: "Jona", long_name: "Das Buch Jona"}),
  Book.new({name: "Micha", id: 40, abbr: "Mi", long_name: "Das Buch Micha"}),
  Book.new({name: "Nahum", id: 41, abbr: "Nah", long_name: "Das Buch Nahum"}),
  Book.new({name: "Habakuk", id: 42, abbr: "Hab", long_name: "Das Buch Habakuk"}),
  Book.new({name: "Zefanja", id: 43, abbr: "Zef", long_name: "Das Buch Zefanja"}),
  Book.new({name: "Haggai", id: 44, abbr: "Hag", long_name: "Das Buch Haggai"}),
  Book.new({name: "Sacharja", id: 45, abbr: "Sach", long_name: "Das Buch Sacharja"}),
  Book.new({name: "Maleachi", id: 46, abbr: "Mal", long_name: "Das Buch Maleachi"}),
  Book.new({name: "Lukas", id: 49, abbr: "Lk", long_name: "Lukas"}),
  Book.new({name: "Johannes", id: 50, abbr: "Joh", long_name: "Johannes"}),
  Book.new({name: "Apostelgeschichte", id: 51, abbr: "Apg", long_name: "Die Apostelgeschichte"}),
  Book.new({name: "Der Brief an die Römer", id: 52, abbr: "Röm", long_name: "Der Brief an die Römer"}),
  Book.new({name: "Der erste Brief an die Korinther", id: 53, abbr: "1 Kor", long_name: "Der erste Brief an die Korinther"}),
  Book.new({name: "Der zweite  Brief an die Korinther", id: 54, abbr: "2 Kor", long_name: "Der zweite  Brief an die Korinther"}),
  Book.new({name: "Der Brief an die Galater", id: 55, abbr: "Gal", long_name: "Der Brief an die Galater"}),
  Book.new({name: "Der Brief an die Epheser", id: 56, abbr: "Eph", long_name: "Der Brief an die Epheser"}),
  Book.new({name: "Der Brief an die Philipper", id: 57, abbr: "Phil", long_name: "Der Brief an die Philipper"}),
  Book.new({name: "Der Brief an die Kolosser", id: 58, abbr: "Kol", long_name: "Der Brief an die Kolosser"}),
  Book.new({name: "Der erste Brief an die Thessaloniker", id: 59, abbr: "1 Thess", long_name: "Der erste Brief an die Thessaloniker"}),
  Book.new({name: "Der zweite Brief an die Thessalonik", id: 60, abbr: "2 Thess", long_name: "Der zweite Brief an die Thessaloniker"}),
  Book.new({name: "Der erste Brief an Timotheus", id: 61, abbr: "1 Tim", long_name: "Der erste Brief an Timotheus"}),
  Book.new({name: "Der zweite Brief an Timotheus", id: 62, abbr: "2 Tim", long_name: "Der zweite Brief an Timotheus"}),
  Book.new({name: "Der Brief an Titus", id: 63, abbr: "Tit", long_name: "Der Brief an Titus"}),
  Book.new({name: "Der Brief an Philemon", id: 64, abbr: "Phlm", long_name: "Der Brief an Philemon"}),
  Book.new({name: "Der Brief an die Hebräer", id: 65, abbr: "Hebr", long_name: "Der Brief an die Hebräer"}),
  Book.new({name: "Der Brief des Jakobus", id: 66, abbr: "Jak", long_name: "Der Brief des Jakobus"}),
  Book.new({name: "Der erste Brief des Petrus", id: 67, abbr: "1 Petr", long_name: "Der erste Brief des Petrus"}),
  Book.new({name: "Der zweite Brief des Petrus", id: 68, abbr: "2 Petr", long_name: "Der zweite Brief des Petrus"}),
  Book.new({name: "Der erste Brief des Johannes", id: 69, abbr: "1 Joh", long_name: "Der erste Brief des Johannes"}),
  Book.new({name: "Der zweite Brief des Johannes", id: 70, abbr: "2 Joh", long_name: "Der zweite Brief des Johannes"}),
  Book.new({name: "Der dritte Brief des Johannes", id: 71, abbr: "3 Joh", long_name: "Der dritte Brief des Johannes"}),
  Book.new({name: "Der Brief des Judas", id: 72, abbr: "Jud", long_name: "Der Brief des Judas"}),
  Book.new({name: "Die Offenbarung", id: 73, abbr: "Offb", long_name: "Die Offenbarung"}),
  Book.new({name: "Matthäus", id: 47, abbr: "Mt", long_name: "Matthäus"}),
  Book.new({name: "Markus", id: 48, abbr: "Mk", long_name: "Markus"}),
]
Book.import(books)
# books.each do |book|
#   book.save!
# end

Pericope.new({book_id: 1, verse_start: "1,1", verse_end: "1,10", title: "test", level: 1, labels: ["Schöpfung"], author: "admin"}).save!
